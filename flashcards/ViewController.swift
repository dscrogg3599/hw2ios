//
//  ViewController.swift
//  flashcards
//
//  Created by Daniel Scroggins on 1/25/22.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //flashcard test
        print("Flashcard Test")
        let c: Flashcard = Flashcard(f: "test", b: "testing")
        let cards = c.genFlashCards()
        for card in cards {
            print(card.flashcardToString())
        }
        //flashcardset test
        print("\n\n\n\nFlashcardSet Test")
        let s: FlashcardSet = FlashcardSet(t: "test")
        let sets = s.genFlashcardSet()
        for set in sets {
            print(set.flashcardSetToString())
        }
        
        
        
    }
}


