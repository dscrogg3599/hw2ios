//
//  FlashcardSet.swift
//  flashcards
//
//  Created by Daniel Scroggins on 1/27/22.
//

import Foundation

class FlashcardSet {
    
    var title: String = ""
    
    
    init(t: String) {
        self.title = t
    }
    
    func genFlashcardSet() -> [FlashcardSet] {
        let sets = [
            FlashcardSet(t: "1"),
            FlashcardSet(t: "2"),
            FlashcardSet(t: "3"),
            FlashcardSet(t: "4"),
            FlashcardSet(t: "5"),
            FlashcardSet(t: "6"),
            FlashcardSet(t: "7"),
            FlashcardSet(t: "8"),
            FlashcardSet(t: "9"),
            FlashcardSet(t: "10")
        ]
        return sets
    }
    
    func flashcardSetToString() -> String {
        let output = self.title
        return output
    }
}
