//
//  Flashcard.swift
//  flashcards
//
//  Created by Daniel Scroggins on 1/27/22.
//

import Foundation

class Flashcard {
    var term: String = ""
    var definition: String = ""
    
    init(f: String, b: String) {
        self.term = f
        self.definition = b
    }
    
    func genFlashCards() -> [Flashcard]{
        
        let flashcards = [
            Flashcard(f: "H", b: "Hydrogen"),
            Flashcard(f: "He", b: "Helium"),
            Flashcard(f: "Li", b: "Lithium"),
            Flashcard(f: "Be", b: "Beryllium"),
            Flashcard(f: "B", b: "Boron"),
            Flashcard(f: "C", b: "Carbon"),
            Flashcard(f: "N", b: "Nitrogen"),
            Flashcard(f: "O", b: "Oxygen"),
            Flashcard(f: "F", b: "Fluorine"),
            Flashcard(f: "Ne", b: "Neon")
        ]
        return flashcards
        
    }
    
    func flashcardToString() -> String {
        let output: String = self.term + ", " + self.definition
        return output
    }
}
